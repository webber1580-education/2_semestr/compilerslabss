using System.Collections.Generic;
using Antlr4.Runtime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RubyCompiler;
using RubyCompiler.RubyCompiler;
using RubyCompiler.RubyCompiler.Constants;
using RubyCompiler.RubyCompiler.Cycles;
using RubyCompiler.RubyCompiler.Dynamic;
using RubyCompiler.RubyCompiler.Function;
using RubyCompiler.RubyCompiler.IfElse;
using RubyCompiler.RubyCompiler.Results;

namespace Test.RubyCompiler
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestIntResult()
        {
            var inputString = @"1 + 2 * 3";
            var inputStream = new AntlrInputStream(inputString);
            var rubyLexer = new RubyLexer(inputStream);
            var commonTokenStream = new CommonTokenStream(rubyLexer);
            var rubyParser = new RubyParser(commonTokenStream);
            var visitor = new RubyCompilerVisitor();
            var tree = visitor.Visit(rubyParser.prog());
            
            var right2 = new IntConstant(3);
            var left2 = new IntConstant(2);
            var sign2 = "*";
            var right1 = new IntResult(left2, sign2, right2);
            var left1 = new IntConstant(1);
            var sign1 = "+";
            var resultTree = new IntResult(left1, sign1, right1);
            // Assert.Equals(tree, resultTree);
        }
        
        [TestMethod]
        public void TestAssignment()
        {
            var inputString = @"a = 5";
            var inputStream = new AntlrInputStream(inputString);
            var rubyLexer = new RubyLexer(inputStream);
            var commonTokenStream = new CommonTokenStream(rubyLexer);
            var rubyParser = new RubyParser(commonTokenStream);
            var visitor = new RubyCompilerVisitor();
            var tree = visitor.Visit(rubyParser.prog());
            
            var right = new IntConstant(5);
            var variableName = "a";
            var sign = "=";
            var resultTree = new Assignment(variableName, sign, right);
            // Assert.Equals(tree, resultTree);
        }
        
        [TestMethod]
        public void TestIf()
        {
            var inputString = @"
            if x > 5
                x = 10
            end";
            var inputStream = new AntlrInputStream(inputString);
            var rubyLexer = new RubyLexer(inputStream);
            var commonTokenStream = new CommonTokenStream(rubyLexer);
            var rubyParser = new RubyParser(commonTokenStream);
            var visitor = new RubyCompilerVisitor();
            var tree = visitor.Visit(rubyParser.prog());
            
            var left1 = new Variable("x");
            var sign1 = ">";
            var right1 = new IntConstant(5);
            var condition = new Condition(left1, sign1, right1);

            var left2 = "x";
            var sign2 = "=";
            var right2 = new IntConstant(10);
            var expression = new Assignment(left2, sign2, right2);
            var expressions = new List<AstNode>();
            expressions.Add(expression);
            var body = new ExpressionList(expressions);
            
            var resultTree = new IfStatement(condition, body, null);
            // Assert.Equals(tree, resultTree);
        }
        
        [TestMethod]
        public void TestFor()
        {
            var inputString = @"
            for i in 10
                x = 5
            end";
            var inputStream = new AntlrInputStream(inputString);
            var rubyLexer = new RubyLexer(inputStream);
            var commonTokenStream = new CommonTokenStream(rubyLexer);
            var rubyParser = new RubyParser(commonTokenStream);
            var visitor = new RubyCompilerVisitor();
            var tree = visitor.Visit(rubyParser.prog());
            
            var variable = new Variable("i");
            var from = new IntConstant(0);
            var to = new IntConstant(10);
            var forHeader = new ForHeader(variable, from, to);
            
            var right = new IntConstant(5);
            var expression = new Assignment("x", "=", right);
            var expressions = new List<AstNode>();
            expressions.Add(expression);
            var expressionList = new ExpressionList(expressions);
            var body = new StatementBody(expressionList);
            
            var resultTree = new ForCycle(forHeader, body);
            // Assert.Equals(tree, resultTree);
        }
    }
}
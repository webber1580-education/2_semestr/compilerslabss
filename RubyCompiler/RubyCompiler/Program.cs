﻿using System;
using System.IO;
using Antlr4.Runtime;
using Antlr4.Runtime.Tree;
using Newtonsoft.Json;

namespace RubyCompiler
{
    class Program
    {
        static void Main(string[] args)
        {
            var filename = Path.Combine(Environment.CurrentDirectory, "TestFiles\\test1.rb");
            using (var file = new StreamReader(filename))
            {
                var inputStream = new AntlrInputStream(file.ReadToEnd());
                var rubyLexer = new RubyLexer(inputStream);
                var commonTokenStream = new CommonTokenStream(rubyLexer);
                var rubyParser = new RubyParser(commonTokenStream);
                
                var visitor = new RubyCompilerVisitor();
                var sourceTree = rubyParser.prog();
                var tree = visitor.Visit(rubyParser.prog());
            }

            Console.WriteLine("All ok!");
            Console.ReadKey();
        }
    }
}
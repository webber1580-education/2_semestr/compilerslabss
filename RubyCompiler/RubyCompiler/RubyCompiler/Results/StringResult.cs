﻿namespace RubyCompiler.RubyCompiler.Results
{
    public class StringResult : Result
    {
        public StringResult(AstNode left, string sign, AstNode right) : base(left, sign, right) { }
    }
}
﻿namespace RubyCompiler.RubyCompiler.Results
{
    public class Result : AstNode
    {
        public AstNode Left { get; set; }

        public string Sign { get; set; }

        public AstNode Right { get; set; }
        
        public Result(AstNode left, string sign, AstNode right)
        {
            Left = left;
            Sign = sign;
            Right = right;
        }
    }
}
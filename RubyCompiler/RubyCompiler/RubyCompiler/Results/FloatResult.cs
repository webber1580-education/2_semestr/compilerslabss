﻿namespace RubyCompiler.RubyCompiler.Results
{
    public class FloatResult : Result
    {
        public FloatResult(AstNode left, string sign, AstNode right) : base(left, sign, right) { }
    }
}
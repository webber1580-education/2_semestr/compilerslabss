﻿namespace RubyCompiler.RubyCompiler.Results
{
    public class IntResult : Result
    {
        public IntResult(AstNode left, string sign, AstNode right) : base(left, sign, right) { }
    }
}
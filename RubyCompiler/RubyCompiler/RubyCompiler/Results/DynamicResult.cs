﻿namespace RubyCompiler.RubyCompiler.Results
{
    public class DynamicResult : Result
    {
        public DynamicResult(AstNode left, string sign, AstNode right) : base(left, sign, right) { }
    }
}
﻿using System.Collections.Generic;

 namespace RubyCompiler.RubyCompiler
{
    public class ExpressionList : AstNode 
    {
        public List<AstNode> Expressions { get; set; }

        public ExpressionList(List<AstNode> expressions)
        {
            Expressions = expressions;
        }
    }
}
﻿namespace RubyCompiler.RubyCompiler
{
    public class Assignment : AstNode
    {
        public string VariableName { get; set; }
        public string Sign { get; set; }
        public AstNode Result { get; set; }

        public Assignment(string variableName, string sign, AstNode result)
        {
            VariableName = variableName;
            Sign = sign;
            Result = result;
        }
    }
}
﻿namespace RubyCompiler.RubyCompiler.Constants
{
    public class FloatConstant : AstNode
    {
        public float Value { get; set; }

        public FloatConstant(float value)
        {
            Value = value;
        }
    }
}
﻿namespace RubyCompiler.RubyCompiler.Constants
{
    public class StringConstant : AstNode
    {
        public string Value { get; set; }

        public StringConstant(string value)
        {
            Value = value;
        }
    }
}
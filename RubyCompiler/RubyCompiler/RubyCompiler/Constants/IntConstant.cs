﻿namespace RubyCompiler.RubyCompiler.Constants
{
    public class IntConstant : AstNode
    {
        public int Value { get; set; }

        public IntConstant(int value)
        {
            Value = value;
        }
    }
}
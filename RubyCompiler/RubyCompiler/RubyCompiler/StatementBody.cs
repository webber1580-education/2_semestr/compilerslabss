﻿using System.Collections.Generic;

namespace RubyCompiler.RubyCompiler
{
    public class StatementBody : AstNode
    {
        public AstNode Body { get; set; }

        public StatementBody(AstNode body)
        {
            Body = body;
        }
    }
}
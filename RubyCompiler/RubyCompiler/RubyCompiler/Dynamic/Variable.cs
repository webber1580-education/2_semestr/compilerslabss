﻿namespace RubyCompiler.RubyCompiler.Dynamic
{
    public class Variable : AstNode
    {
        public string Name { get; set; }

        public Variable(string name)
        {
            Name = name;
        }
    }
}
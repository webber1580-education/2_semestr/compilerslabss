﻿namespace RubyCompiler.RubyCompiler.Cycles
{
    public class ForCycle : AstNode
    {
        public ForHeader ForHeader { get; set; }

        public StatementBody Body { get; set; }

        public ForCycle(ForHeader forHeader, StatementBody body)
        {
            ForHeader = forHeader;
            Body = body;
        }
    }
}
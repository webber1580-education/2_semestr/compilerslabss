﻿using RubyCompiler.RubyCompiler.Dynamic;

namespace RubyCompiler.RubyCompiler.Cycles
{
    public class ForHeader : AstNode
    {
        public Variable Variable { get; set; }

        public AstNode From { get; set; }

        public AstNode To { get; set; }

        public ForHeader(Variable variable, AstNode from, AstNode to)
        {
            Variable = variable;
            From = from;
            To = to;
        }
    }
}
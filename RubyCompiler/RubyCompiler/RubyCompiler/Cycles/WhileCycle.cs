﻿namespace RubyCompiler.RubyCompiler.Cycles
{
    public class WhileCycle : AstNode
    {
        public AstNode Condition { get; set; }

        public AstNode Body { get; set; }
        
        public WhileCycle(AstNode condition, AstNode body)
        {
            Condition = condition;
            Body = body;
        }
    }
}
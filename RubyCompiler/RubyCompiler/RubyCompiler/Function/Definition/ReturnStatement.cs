﻿namespace RubyCompiler.RubyCompiler.Function
{
    public class ReturnStatement : AstNode
    {
        public AstNode Return { get; set; }

        public ReturnStatement(AstNode returnValue)
        {
            Return = returnValue;
        }
    }
}
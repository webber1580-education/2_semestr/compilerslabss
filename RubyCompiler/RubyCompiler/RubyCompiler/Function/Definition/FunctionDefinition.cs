﻿using System.Collections.Generic;

namespace RubyCompiler.RubyCompiler.Function
{
    public class FunctionDefinition : AstNode
    {
        public AstNode Header { get; set; }

        public AstNode Body { get; set; }

        public FunctionDefinition(AstNode header, AstNode body)
        {
            Header = header;
            Body = body;
        }    
    }
}
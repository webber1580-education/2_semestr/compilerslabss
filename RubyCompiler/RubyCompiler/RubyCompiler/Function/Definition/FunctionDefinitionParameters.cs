﻿using System.Collections.Generic;

namespace RubyCompiler.RubyCompiler.Function
{
    public class FunctionDefinitionParameters : AstNode
    {
        public List<string> Parameters { get ; set; }
        
        public FunctionDefinitionParameters(List<string> parameters)
        {
            Parameters = parameters;
        }
    }
}
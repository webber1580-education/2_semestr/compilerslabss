﻿using System.Collections.Generic;

namespace RubyCompiler.RubyCompiler.Function
{
    public class FunctionHeader : AstNode
    {
        public string Name { get ;set; }
        
        public FunctionDefinitionParameters DefinitionParameters { get ;set; }
        
        public FunctionHeader(string name, FunctionDefinitionParameters definitionParameters)
        {
            Name = name;
            DefinitionParameters = definitionParameters;
        }    
    }
}
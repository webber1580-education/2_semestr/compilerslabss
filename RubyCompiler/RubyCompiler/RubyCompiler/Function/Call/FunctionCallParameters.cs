﻿using System.Collections.Generic;

namespace RubyCompiler.RubyCompiler.Function
{
    public class FunctionCallParameters: AstNode
    {
        public List<AstNode> Parameters { get ; set; }
        
        public FunctionCallParameters(List<AstNode> parameters)
        {
            Parameters = parameters;
        }
    }
}
﻿namespace RubyCompiler.RubyCompiler.Function
{
    public class FunctionCall : AstNode
    {
        public string Name { get ; set; }
        
        public FunctionCallParameters Parameters { get ; set; }
        
        public FunctionCall(string name, FunctionCallParameters definitionParameters)
        {
            Name = name;
            Parameters = definitionParameters;
        }    
    }
}
﻿namespace RubyCompiler.RubyCompiler
{
    public class Condition : AstNode
    {
        public AstNode Left { get; set; }
        
        public string Sign { get; set; }

        public AstNode Right { get; set; }

        public Condition(AstNode left, string sign, AstNode right)
        {
            Left = left;
            Sign = sign;
            Right = right;
        }
    }
}
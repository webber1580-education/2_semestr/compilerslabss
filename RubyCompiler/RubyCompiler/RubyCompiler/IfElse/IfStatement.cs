﻿namespace RubyCompiler.RubyCompiler.IfElse
{
    public class IfStatement : AstNode
    {
        public AstNode Condition { get; set; }

        public AstNode IfBody { get; set; }

        public AstNode ElseBody { get; set; }

        public IfStatement(AstNode condition, AstNode ifBody, AstNode elseBody)
        {
            Condition = condition;
            IfBody = ifBody;
            ElseBody = elseBody;
        }
    }
}
﻿namespace RubyCompiler.RubyCompiler.IfElse
{
    public class ElsifStatement : AstNode
    {
        public AstNode Condition { get; set; }

        public AstNode ElsifBody { get; set; }

        public AstNode ElseBody { get; set; }

        public ElsifStatement(AstNode condition, AstNode elsifBody, AstNode elseBody)
        {
            Condition = condition;
            ElsifBody = elsifBody;
            ElseBody = elseBody;
        }
    }
}
﻿namespace RubyCompiler.RubyCompiler.IfElse
{
    public class UnlessStatement : AstNode
    {
        public AstNode Condition { get; set; }

        public AstNode UnlessBody { get; set; }

        public AstNode ElseBody { get; set; }

        public UnlessStatement(AstNode condition, AstNode unlessBody, AstNode elseBody)
        {
            Condition = condition;
            UnlessBody = unlessBody;
            ElseBody = elseBody;
        }
    }
}

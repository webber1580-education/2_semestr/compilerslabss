﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Antlr4.Runtime;
using RubyCompiler.RubyCompiler;
using RubyCompiler.RubyCompiler.Constants;
using RubyCompiler.RubyCompiler.Cycles;
using RubyCompiler.RubyCompiler.Dynamic;
using RubyCompiler.RubyCompiler.Function;
using RubyCompiler.RubyCompiler.IfElse;
using RubyCompiler.RubyCompiler.Results;

namespace RubyCompiler
{
    
     public class RubyCompilerVisitor : RubyBaseVisitor<AstNode>
    {
        private string _commentsFile = "TestFiles\\comments.txt";
        private string _sringFile = "TestFiles\\strings.txt";
        
        public override AstNode VisitComment(RubyParser.CommentContext context)
        {
            using (StreamWriter sw = File.AppendText(_commentsFile))
            {
                sw.WriteLine(context.GetChild(0).GetText());
            }

            return null;
        }
        // // // ////////////////////////////
        
        public override AstNode VisitExpression_list(RubyParser.Expression_listContext context)
        {
            AstNode expression;
            var expressionList = new List<AstNode>();
            ParserRuleContext currentContext = context;
            while (currentContext.children.Count == 3)
            {
                expression = Visit(currentContext.GetChild(1));
                expressionList.Insert(0, expression);
                currentContext = (ParserRuleContext) currentContext.GetChild(0);
            } 
            expression = Visit(currentContext.GetChild(0));
            expressionList.Insert(0, expression);
            
            return new ExpressionList(expressionList);
        }

        public override AstNode VisitFunction_call(RubyParser.Function_callContext context)
        {
            if (context.children.Count == 4)
            {
                var name = context.GetChild(0).GetText();
                var parameters = Visit(context.GetChild(2));
                return new FunctionCall(name, (FunctionCallParameters)parameters);
            }
            else if (context.children.Count == 3)
            {
                var name = context.GetChild(0).GetText();
                return new FunctionCall(name, null);
            }
            else
            {
                var name = context.GetChild(0).GetText();
                var parameters = Visit(context.GetChild(1));
                return new FunctionCall(name, (FunctionCallParameters)parameters);
            }
        }

        public override AstNode VisitFunction_call_params(RubyParser.Function_call_paramsContext context)
        {
            AstNode parameter;
            var parametersList = new List<AstNode>();
            ParserRuleContext currentContext = context;
            while (currentContext.children.Count == 3)
            {
                parameter = Visit(currentContext.GetChild(2));
                parametersList.Insert(0, parameter);
                currentContext = (ParserRuleContext) currentContext.GetChild(0);
            } 
            parameter = Visit(currentContext.GetChild(0));
            parametersList.Insert(0, parameter);
            
            return new FunctionCallParameters(parametersList);
        }

        public override AstNode VisitFunction_definition(RubyParser.Function_definitionContext context)
        {
            var header = Visit(context.GetChild(0));
            var body = Visit(context.GetChild(1));
            return new FunctionDefinition(header, body);
        }

        public override AstNode VisitFunction_definition_header(RubyParser.Function_definition_headerContext context)
        {
            if (context.children.Count == 3)
            {
                var name = context.GetChild(1).GetText();
                return new FunctionHeader(name, null);
            }
            else
            {
                var name = context.GetChild(1).GetText();
                var parameters = Visit(context.GetChild(2));
                return new FunctionHeader(name, (FunctionDefinitionParameters)parameters);
            }
        }

        public override AstNode VisitFunction_definition_params(RubyParser.Function_definition_paramsContext context)
        {
            if (context.children.Count == 1)
            {
                return Visit(context.GetChild(0));
            }
            else if (context.children.Count == 2)
            {
                return null;
            }
            else
            {
                return Visit(context.GetChild(1));
            }
        }
        

        public override AstNode VisitFunction_definition_params_list(RubyParser.Function_definition_params_listContext context)
        {
            string parameter;
            var parametersList = new List<string>();
            ParserRuleContext currentContext = context;
            while (currentContext.children.Count == 3)
            {
                parameter = currentContext.GetChild(2).GetText();
                parametersList.Insert(0, parameter);
                currentContext = (ParserRuleContext) currentContext.GetChild(0);
            } 
            parameter = currentContext.GetChild(0).GetText();
            parametersList.Insert(0, parameter);
            
            return new FunctionDefinitionParameters(parametersList);
        }

        public override AstNode VisitReturn_statement(RubyParser.Return_statementContext context)
        {
            var returnValue = Visit(context.GetChild(1));
            return new ReturnStatement(returnValue);
        }

        public override AstNode VisitFor_statement(RubyParser.For_statementContext context)
        {
            var header = Visit(context.GetChild(0));
            var body = Visit(context.GetChild(1));
            return new ForCycle((ForHeader)header, (StatementBody)body);
        }

        public override AstNode VisitFor_header(RubyParser.For_headerContext context)
        {
            if (context.children.Count == 7)
            {
                var variable = Visit(context.GetChild(1));
                var from = Visit(context.GetChild(3));
                var to = Visit(context.GetChild(5));
                return new ForHeader((Variable)variable, from, to);
            }
            else if (context.children.Count == 5)
            {
                var variable = Visit(context.GetChild(1));
                var from = new IntConstant(0);
                var to = Visit(context.GetChild(3));
                return new ForHeader((Variable)variable, from, to);
            }
            else if (context.children.Count == 10)
            {
                var variable = Visit(context.GetChild(7));
                var from = new IntConstant(0);
                var to = Visit(context.GetChild(1));
                return new ForHeader((Variable)variable, from, to);
            }
            else if (context.children.Count == 8)
            {
                var variable = Visit(context.GetChild(5));
                var from = new IntConstant(0);
                var to = Visit(context.GetChild(0));
                return new ForHeader((Variable)variable, from, to);
            }
            else
            {
                var variable = Visit(context.GetChild(9));
                var from = Visit(context.GetChild(1));
                var to = Visit(context.GetChild(3));
                return new ForHeader((Variable)variable, from, to);
            }
        }

        public override AstNode VisitWhile_statement(RubyParser.While_statementContext context)
        {
            var condition = Visit(context.GetChild(1));
            var body = Visit(context.GetChild(3));
            return new WhileCycle(condition, body);
        }

        public override AstNode VisitUnless_statement(RubyParser.Unless_statementContext context)
        {
            if (context.children.Count == 5)
            {
                var condition = Visit(context.GetChild(1));
                var unlessBody = Visit(context.GetChild(3));
                return new UnlessStatement(condition, unlessBody, null);
            }
            else if (context.children.Count == 8)
            {
                var condition = Visit(context.GetChild(1));
                var unlessBody = Visit(context.GetChild(3));
                var elseBody = Visit(context.GetChild(6));
                return new UnlessStatement(condition, unlessBody, elseBody);
            }
            else
            {
                var condition = Visit(context.GetChild(1));
                var unlessBody = Visit(context.GetChild(3));
                var elseBody = Visit(context.GetChild(4));
                return new UnlessStatement(condition, unlessBody, elseBody);
            }
        }

        public override AstNode VisitIf_statement(RubyParser.If_statementContext context)
        {
            if (context.children.Count == 5)
            {
                var condition = Visit(context.GetChild(1));
                var ifBody = Visit(context.GetChild(3));
                return new IfStatement(condition, ifBody, null);
            }
            else if (context.children.Count == 8)
            {
                var condition = Visit(context.GetChild(1));
                var ifBody = Visit(context.GetChild(3));
                var elseBody = Visit(context.GetChild(6));
                return new IfStatement(condition, ifBody, elseBody);
            }
            else
            {
                var condition = Visit(context.GetChild(1));
                var ifBody = Visit(context.GetChild(3));
                var elseBody = Visit(context.GetChild(4));
                return new IfStatement(condition, ifBody, elseBody);
            }
        }

        public override AstNode VisitIf_elsif_statement(RubyParser.If_elsif_statementContext context)
        {
            if (context.children.Count == 4)
            {
                var condition = Visit(context.GetChild(1));
                var elsifBody = Visit(context.GetChild(3));
                return new ElsifStatement(condition, elsifBody, null);
            }
            else if (context.children.Count == 7)
            {
                var condition = Visit(context.GetChild(1));
                var elsifBody = Visit(context.GetChild(3));
                var elseBody = Visit(context.GetChild(6));
                return new ElsifStatement(condition, elsifBody, elseBody);
            }
            else
            {
                var condition = Visit(context.GetChild(1));
                var elsifBody = Visit(context.GetChild(3));
                var elseBody = Visit(context.GetChild(4));
                return new ElsifStatement(condition, elsifBody, elseBody);
            }
        }

        public override AstNode VisitStatement_expression_list(RubyParser.Statement_expression_listContext context)
        {
            AstNode expression;
            var expressionList = new List<AstNode>();
            ParserRuleContext currentContext = context;
            while (currentContext.children.Count == 3)
            {
                if (context.ret == null && context.brk == null)
                {
                    expression = Visit(currentContext.GetChild(1));
                    expressionList.Insert(0, expression);
                }
                currentContext = (ParserRuleContext)currentContext.GetChild(0);
            } 
            if (context.ret == null && context.brk == null)
            {
                expression = Visit(currentContext.GetChild(0));
                expressionList.Insert(0, expression);
            }
            
            var body = new ExpressionList(expressionList);
            
            return new StatementBody(body);
        }

        public override AstNode VisitRvalue(RubyParser.RvalueContext context)
        {
            AstNode result = null;
            if (context.children.Count == 1)
            {
                result = Visit(context.GetChild(0));
            }
            else if (context.op == null)
            {
                result = Visit(context.GetChild(1));
            }
            else if (context.children.Count == 3)
            {
                var leftResult = Visit(context.GetChild(0));
                var rightResult = Visit(context.GetChild(2));
                var sign = context.GetChild(1).GetText();
                
                if (leftResult is DynamicResult || leftResult is Variable || rightResult is DynamicResult || rightResult is Variable)
                    result = new DynamicResult(leftResult, sign, rightResult);
                else if (leftResult is StringResult || leftResult is StringConstant || rightResult is StringResult || rightResult is StringConstant)
                    result =  new StringResult(leftResult, sign, rightResult);
                else if (leftResult is FloatResult || leftResult is FloatConstant || rightResult is FloatResult || rightResult is FloatConstant)
                    result = new FloatResult(leftResult, sign, rightResult);
                else
                    result = new IntResult(leftResult, sign, rightResult);
            }

            return result;
        }

        public override AstNode VisitComparison_list(RubyParser.Comparison_listContext context)
        {
            if (context.op != null)
            {
                var left = Visit(context.GetChild(0));
                var sign = context.GetChild(1).GetText();
                var right = Visit(context.GetChild(2));
                return new Condition(left, sign, right);
            }
            else if (context.children.Count == 1)
            {
                return Visit(context.GetChild(0));
            }
            else
            {
                return Visit(context.GetChild(1));
            }
        }
        

        public override AstNode VisitComparison(RubyParser.ComparisonContext context)
        {
            var left = Visit(context.GetChild(0));
            var sign = context.GetChild(1).GetText();
            var right = Visit(context.GetChild(2));
            return new Condition(left, sign, right);

        }

        public override AstNode VisitDynamic_result(RubyParser.Dynamic_resultContext context)
        {
            if (context.children.Count == 3)
            {
                if (context.op != null)
                {
                    var left = Visit(context.GetChild(0));
                    var right = Visit(context.GetChild(2));
                    var sign = context.GetChild(1).GetText();
                    return new DynamicResult(left, sign, right);
                }
                else
                {
                    return Visit(context.GetChild(1));
                }
            }
            else
            {
                return Visit(context.GetChild(0));
            }
        }

        public override AstNode VisitInt_result(RubyParser.Int_resultContext context)
        {
            if (context.children.Count == 3)
            {
                if (context.op != null)
                {
                    var left = Visit(context.GetChild(0));
                    var right = Visit(context.GetChild(2));
                    var sign = context.GetChild(1).GetText();
                    return new IntResult(left, sign, right);
                }
                else
                {
                    return Visit(context.GetChild(1));
                }
            }
            else if (context.children.Count == 2)
            {
                var value = Int32.Parse(context.GetChild(1).GetText());
                return new IntConstant(value * (-1));
            }
            else
            {
                return Visit(context.GetChild(0));
            }
        }

        public override AstNode VisitFloat_result(RubyParser.Float_resultContext context)
        {
            if (context.children.Count == 3)
            {
                if (context.op != null)
                {
                    var left = Visit(context.GetChild(0));
                    var right = Visit(context.GetChild(2));
                    var sign = context.GetChild(1).GetText();
                    return new FloatResult(left, sign, right);
                }
                else
                {
                    return Visit(context.GetChild(1));
                }
            }
            else if (context.children.Count == 2)
            {
                var value = float.Parse(context.GetChild(1).GetText());
                return new FloatConstant(value * (-1));
            }
            else
            {
                return Visit(context.GetChild(0));
            }
        }

        public override AstNode VisitString_result(RubyParser.String_resultContext context)
        {
            if (context.children.Count == 3)
            {
                var left = Visit(context.GetChild(0));
                var right = Visit(context.GetChild(2));
                var sign = context.GetChild(1).GetText();
                return new StringResult(left, sign, right);
            }
            else
            {
                return Visit(context.GetChild(0));
            }
        }

        public override AstNode VisitDynamic_assignment(RubyParser.Dynamic_assignmentContext context)
        {
            var variableName = context.GetChild(0).GetText();
            var sign = context.GetChild(1).GetText();
            var result = Visit(context.GetChild(2));
            return new Assignment(variableName, sign, result);
        }

        public override AstNode VisitInt_assignment(RubyParser.Int_assignmentContext context)
        {
            var variableName = context.GetChild(0).GetText();
            var sign = context.GetChild(1).GetText();
            var result = Visit(context.GetChild(2));
            return new Assignment(variableName, sign, result);
        }

        public override AstNode VisitFloat_assignment(RubyParser.Float_assignmentContext context)
        {
            var variableName = context.GetChild(0).GetText();
            var sign = context.GetChild(1).GetText();
            var result = Visit(context.GetChild(2));
            return new Assignment(variableName, sign, result);
        }

        public override AstNode VisitString_assignment(RubyParser.String_assignmentContext context)
        {
            var variableName = context.GetChild(0).GetText();
            var sign = context.GetChild(1).GetText();
            var result = Visit(context.GetChild(2));
            return new Assignment(variableName, sign, result);
        }

        public override AstNode VisitInt_t(RubyParser.Int_tContext context)
        {
            var value = Int32.Parse(context.GetChild(0).GetText());
            return new IntConstant(value);
        }

        public override AstNode VisitFloat_t(RubyParser.Float_tContext context)
        {
            var value = float.Parse(context.GetChild(0).GetText(), CultureInfo.InvariantCulture);
            return new FloatConstant(value);
        }

        public override AstNode VisitLiteral_t(RubyParser.Literal_tContext context)
        {
            using (StreamWriter sw = File.AppendText(_sringFile))
            {
                sw.WriteLine(context.GetChild(0).GetText());
            }
            var value = context.GetChild(0).GetText();
            return new StringConstant(value);
        }

        public override AstNode VisitId(RubyParser.IdContext context)
        {
            var name = context.GetChild(0).GetText();
            return new Variable(name);
        }
    }
}